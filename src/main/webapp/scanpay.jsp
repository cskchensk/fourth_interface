<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="/resources/control.js"></script>
    <script type="text/javascript" src="/resources/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/jquery.qrcode.js"></script>
    <title>扫码支付接口</title>
</head>
<body>
<form name="form1" action="/mobpay/1/scanpay">
    <table>
        <tr>扫码支付接口</tr>
        <tr>
            <td>交易订单号：</td>
            <td><input type="text" id="orderId" style="width:160px;"/><b>*</b> <input type="button" value="刷新订单号"
                                                                                      onclick="javascript:setOrderNo(this.form)"/>
            </td>
        </tr>
        <tr>
            <td>机构号：</td>
            <td><input type="text" id="orgId" style="width:160px;" value="1009"/><b>*</b></td>
            <td>商户号：</td>
            <td><input type="text" id="mchtId" style="width:160px;" value="M81017000845"/><b>*</b></td>
        </tr>
        <tr>
            <td>交易时间：</td>
            <td><input type="text" id="transTime" style="width:160px;"/><b>*</b></td>
            <td>受理平台代码：</td>
            <td><select id="transChannel">
                <option value="01">微信</option>
                <option value="02">支付宝</option>
                <option value="05">银联H5</option>
                <option value="06">QQ钱包</option>
            </select>
        </tr>
        <tr>
            <td>前台跳转地址：</td>
            <td><input type="text" id="frontUrl" style="width:160px;" value="https://www.baidu.com"/><b>*</b></td>
        </tr>
        <tr>
            <td>后台通知地址：</td>
            <td><input type="text" id="notifyUrl" style="width:160px;" value="https://www.baidu.com"/><b>*</b></td>
            <td>交易金额(分)：</td>
            <td><input type="text" id="tranAmt" style="width:160px;" value="1000"/><b>*</b></td>
        </tr>
        <tr>
            <td>币种：</td>
            <td><input type="text" id="currency" style="width:160px;" value="JPY"/><b>*</b></td>
            <td>备注/摘要：</td>
            <td><input type="text" id="remark" style="width:160px;" value="3"/><b>*</b></td>
        </tr>
        <tr>
            <td>设备号：</td>
            <td><input type="text" id="deviceInfo" style="width:160px;" value="1"/><b></b></td>
            <td>终端IP：</td>
            <td><input type="text" id="deviceIp" style="width:160px;" value="2"/><b></b></td>
        </tr>
        <tr>
            <td>商品描述：</td>
            <td><input type="text" id="goodsName" style="width:160px;" value="1"/><b>*</b></td>
            <td>商品标签：</td>
            <td><input type="text" id="goodsTag" style="width:160px;" value="1"/><b>*</b></td>
        </tr>
    </table>
    <input type="button" id="confirm" value="提交">
</form>
<div id="code">
</div>
<div><textarea rows="10" cols="80" id="result"></textarea></div>
</body>
<script type="text/javascript">
    //初始化订单号
    setOrderNo(document.form1);
    $(function () {
        $('#confirm').click(function () {
            $.ajax({
                type: "POST",
                url: "/scanpay",
                timeout: 5000,
                dataType: 'json',
                data: {
                    'orderId': $('#orderId').val(),
                    'orgId': $('#orgId').val(),
                    'mchtId': $('#mchtId').val(),
                    'transTime': $('#transTime').val(),
                    'transChannel': $('#transChannel').val(),
                    'frontUrl': $('#frontUrl').val(),
                    'notifyUrl': $('#notifyUrl').val(),
                    'tranAmt': $('#tranAmt').val(),
                    'currency': $('#currency').val(),
                    'remark': $('#remark').val(),
                    'deviceInfo': $('#deviceInfo').val(),
                    'deviceIp': $('#deviceIp').val(),
                    'goodsName': $('#goodsName').val(),
                    'goodsTag': $('#goodsTag').val()
                },
                success: function (data) {
                    $('#result').val(JSON.stringify(data))
                    var codeUrl = data.codeUrl;
                    $('#code').qrcode(codeUrl);
                }
            })
        })
    })
</script>

</html>