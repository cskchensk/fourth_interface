<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="/resources/control.js"></script>
    <script type="text/javascript" src="/resources/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/jquery.qrcode.js"></script>
</head>
<body>
<form name="form1" action="/mobpay/1/scanpay">
    <table>
        <tr>代付交易</tr>
        <tr>
            <td>交易订单号：</td>
            <td><input type="text" id="orderId" style="width:160px;"/><b>*</b> <input type="button" value="刷新订单号"
                                                                                      onclick="javascript:setOrderNo(this.form)"/>
            </td>
            <td>证件号：</td>
            <td><input type="text" id="idnum" style="width:160px;" value="511526198708220913"/><b>*</b></td>
        </tr>
        <tr>
            <td>机构号：</td>
            <td><input type="text" id="orgId" style="width:160px;" value="A70706000188"/><b>*</b></td>
            <td>商户号：</td>
            <td><input type="text" id="mchtId" style="width:160px;" value="M81017000845"/><b>*</b></td>
        </tr>
        <tr>
            <td>交易时间：</td>
            <td><input type="text" id="transTime" style="width:160px;"/><b>*</b></td>
            <td>证件类型：</td>
            <td><input type="text" id="idtype" style="width:160px;" value="1"/><b>*</b></td>
        </tr>
        <tr>
            <td>交易金额(分)：</td>
            <td><input type="text" id="transAmt" style="width:160px;" value="300"/><b>*</b></td>
            <td>结算银行：</td>
            <td><input type="text" id="settleBank" style="width:160px;" value="宁波银行"/><b>*</b></td>
        </tr>
        <tr>
            <td>币种：</td>
            <td><input type="text" id="currency" style="width:160px;" value="CNY"/><b>*</b></td>
            <td>备注/摘要：</td>
            <td><input type="text" id="remark" style="width:160px;" value="备注"/><b>*</b></td>
        </tr>
        <tr>
            <td>结算卡号：</td>
            <td><input type="text" id="settlePan" style="width:160px;" value="6217734701348294"/><b>*</b></td>
            <td>姓名：</td>
            <td><input type="text" id="name" style="width:160px;" value="柳培兵"/><b>*</b></td>
        </tr>
        <tr>
            <td>手机号：</td>
            <td><input type="text" id="mobile" style="width:160px;" value="13586573022"/><b>*</b></td>
        </tr>
        <%--<tr>--%>
            <%--<td>签名：</td>--%>
            <%--<td><input type="text" id="sign" style="width:160px;" value="123321"/><b>*</b></td>--%>
        <%--</tr>--%>




    </table>
    <input type="button" id="confirm" value="提交">
</form>
<div id="code">
</div>
<div><textarea rows="10" cols="80" id="result"></textarea></div>
</body>
<script type="text/javascript">
    //初始化订单号
    setOrderNo(document.form1);
    $(function () {
        $('#confirm').click(function () {

            $.ajax({
                type: "POST",
                url: "/withdraw",
                scriptCharset: 'utf-8',
                timeout: 5000,
                data:{
                    'mchtOrderId': $('#orderId').val(),
                    'idnum': $('#idnum').val(),
                    'orgId': $('#orgId').val(),
                    'mchtId': $('#mchtId').val(),
                    'transTime': $('#transTime').val(),
                    'tranAmt': $('#transAmt').val(),
                    'mobile': $('#mobile').val(),
                    'currency': $('#currency').val(),
                    'remark': $('#remark').val(),
                    'settlePan': $('#settlePan').val(),
                    'settleBank': $('#settleBank').val(),
                    'sign': $('#sign').val(),
                    'name': $('#name').val()

                },
                success: function (data) {
                    $('#result').val(JSON.stringify(data));
                }
            })
        })
    })
</script>
</html>