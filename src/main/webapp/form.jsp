<%--
  Created by IntelliJ IDEA.
  User: chensk
  Date: 2018/8/6
  Time: 下午5:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="/resources/vue.min.js"></script>
    <script type="text/javascript" src="/resources/axios.min.js"></script>
    <script type="text/javascript" src="/resources/qs.min.js"></script>
</head>
<body id="vues">

<form name="form" action="/pcsubmit" method="post">
    PC支付接口<table>
    <tbody><tr></tr>
    <tr>
        <td>交易订单号：</td>
        <td><input type="text" name="orderId" value="{{orderId}}"  style="width:160px;"><b>*</b> <input type="button" value="刷新订单号"  v-on:click="setOrderNo" >
        </td>
    </tr>
    <tr>
        <td>用户在商户唯一id(transChannel为04 快捷支付 时传 否则不传 )：</td>
        <td><input type="text" name="userId" style="width:160px;" value="KW000002"><b>*</b></td>

    </tr>
    <tr>

        <td>商户号：</td>
        <td><input type="text" name="mchtId" style="width:160px;" value="{{mchtId}}"><b>*</b></td>
    </tr>


    <tr>
        <td>交易时间：</td>
        <td><input type="text" name="transTime"  value="{{transTime}}" style="width:160px;"><b>*</b></td>
        <td>受理平台代码：</td>
        <td><select name="transChannel">
            <option value="04">快捷</option>

            <option value="07">PC</option>
            <%--<option value="02">支付宝</option>--%>
        </select>
        </td></tr>
    <tr>
        <td>前台跳转地址：</td>
        <td><input type="text" name="frontUrl" style="width:160px;" value="https://www.baidu.com"><b>*</b></td>
    </tr>
    <tr>
        <td>后台通知地址：</td>
        <td><input type="text" name="notifyUrl" style="width:160px;" value="https://www.baidu.com"><b>*</b></td>
        <td>交易金额(分)：</td>
        <td><input type="text" name="tranAmt" style="width:160px;" value="1000"><b>*</b></td>
    </tr>

    <tr>
        <td>IP：</td>
        <td><input type="text" name="deviceIp" style="width:160px;" value="183.14.28.60"><b>*</b></td>
    </tr>
      <%--<tr>--%>
    <%--<td>银行代码：</td>--%>
    <%--<td><input type="text" name="bankCode" style="width:160px;" value="01020000"><b>*</b></td>--%>
    <%--</tr>--%>

   <tr>
        <td>商品描述：</td>
        <td><input type="text" name="goodsName" style="width:160px;" value="狗头"><b>*</b></td>
        <td>商品标签：</td>
        <td><input type="text" name="goodsTag" style="width:160px;" value="狗头"><b>*</b></td>
    </tr>
    <tr>
        <td>：支付方式（因为上游改版 transChannel为07时 必须传1  不然需要前台传入银行卡而且只支持5种 非常麻烦）</td>
        <td>
            <select name="payMode">
                <option value="1">WAP</option>
            </select>
        </td>

    </tr>
    </tbody></table>
    <input type="submit" id="confirm" value="构建表单">
</form>

<script>
    var vue = new Vue({
        el: '#vues',
        data: {
            mchtId: 'M81017000845',
            orderId:'',
            transTime:''
        },
        methods: {
            setOrderNo: function (event) {

                var curr = new Date();
                var m = curr.getMonth() + 1;
                if (m < 10) {
                    m = '0' + m;
                }
                var d = curr.getDate();
                if (d < 10) {
                    d = '0' + d;
                }
                var h = curr.getHours();
                if (h < 10) {
                    h = '0' + h;
                }
                var mi = curr.getMinutes();
                if (mi < 10) {
                    mi = '0' + mi;
                }
                var s = curr.getSeconds();
                if (s < 10) {
                    s = '0' + s;
                }
                this.transTime = '' + curr.getFullYear() + m + d + h + mi + s;
                this.orderId = '' + curr.getFullYear() + m + d + h + mi + s;
            }


        }}

    ).setOrderNo();

</script>
</body>
</html>
