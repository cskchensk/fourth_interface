/**
* 设置表单订单信息.
*/
function setOrderNo(theForm) {
var curr = new Date();
var m = curr.getMonth() + 1;
if (m < 10) {
m = '0' + m;
}
var d = curr.getDate();
if (d < 10) {
d = '0' + d;
}
var h = curr.getHours();
if (h < 10) {
h = '0' + h;
}
var mi = curr.getMinutes();
if (mi < 10) {
mi = '0' + mi;
}
var s = curr.getSeconds();
if (s < 10) {
s = '0' + s;
}
theForm.transTime.value = '' + curr.getFullYear() + m + d + h + mi + s;
theForm.orderId.value =  '' +curr.getFullYear() + m + d + h + mi + s;
}

/**
* 选择Server事件.
*/
function changeServer() {
var form1 = document.form1;
form1.serverIp.value = form1.sltServerIp.value;
form1.notifyUrl.value = form1.notifyUrl.value.replace(/.*?idemo/,
form1.serverIp.value + '/idemo');
form1.bgNotifyUrl.value = form1.bgNotifyUrl.value.replace(/.*?idemo/,
form1.serverIp.value + '/idemo');
}

/**
* 设置渠道的测试数据.
*
* @param channelId
*/
function resetTestData(channelId) {
var form1 = document.form1;

form1.acctCat.value = "01";
form1.idType.value = "01";

switch (channelId){
case 'RESET':
form1.ibankCode.value = '00000000';
form1.acctNo.value = "";
form1.acctName.value = "";
form1.idNo.value = "";
break;
case '03090000CIB': // 兴业银行
form1.ibankCode.value = '03090000';
form1.acctNo.value = "622909119013067013";
form1.acctName.value = "huangxun";
form1.idNo.value = "110101199001013590";
break;
case '04145210STD': // 汉口银行
form1.ibankCode.value = '04145210';
form1.acctNo.value = "6223250007268633";
form1.acctName.value = "测试";
form1.idNo.value = "210122198210120326";
break;
case '04012900BOS': // 上海银行
form1.ibankCode.value = '04012900';
form1.acctNo.value = "622468101006187621";
form1.acctName.value = "测试客户";
form1.idNo.value = "310109198702230516";
break;
case '04123330WZCB': // 温州银行
form1.ibankCode.value = '04123330';
form1.acctNo.value = "6219770106593967";
form1.acctName.value = "朱秋林";
form1.idNo.value = "430281198209179124";
break;
case '03010000MBS':// MBS交行
form1.ibankCode.value = '03010000';
form1.idNo.value = "330103760214071";
if (form1.transCode.value == '1001' || form1.transCode.value == '2001') {// 实名认证、扣款
form1.acctNo.value = "6222600170003143111";
form1.acctName.value = "交行客户";

} else if (form1.transCode.value == '2002') {// 付款
form1.acctNo.value = "6222600170008213794";
form1.acctName.value = "沈刚";
}
break;
case '01030000MBS': //MBS农行
form1.ibankCode.value = '01030000';
form1.acctNo.value = "6228480030162909912";
form1.acctName.value = "简芳旭";
form1.idNo.value = "320831196807170538";
break;
case '01050000MBS': // MBS建行
form1.ibankCode.value = '01050000';
form1.acctNo.value = "4367423761000283064";
form1.acctName.value = "李二九";
form1.idNo.value = "310104199009098372";
break;
case '01030000FPS': //FPS农行签约
form1.ibankCode.value = '01030000';
form1.acctNo.value = "6228480031597685614";
form1.acctName.value = "刘梦烨";
form1.idNo.value = "15230119900429202X";
break;
case '01050000FPS': // FPS建行签约
form1.ibankCode.value = '01050000';
form1.acctNo.value = "6227001215551088822";
form1.acctName.value = "崔琦东";
form1.idNo.value = "310109198709141030";
break;
case '00001003UHN':// 银联
form1.ibankCode.value = '00001003';
form1.acctNo.value = "6222023602899998372";
form1.acctName.value = "互联网";
form1.idNo.value = "310104199009098372";
break;
case '03080000EBPP':// 银联EBPP招行
form1.ibankCode.value = '03080000';
form1.acctNo.value = "9555552120000001";
form1.acctName.value = "清清";
form1.idNo.value = "510265790128567";
form1.phoneNo.value = "15611326811";
break;
case '03060000CGB':// CGB -广发协议支付
form1.ibankCode.value = '03060000';
form1.acctNo.value = "6225683521000368429";
form1.acctName.value = "布衣";
form1.idNo.value = "440103198001010116";
form1.phoneNo.value = "13101316948";
break;
case '00001010TLT':// TLT-通联通4通联宝
form1.ibankCode.value = '0105';
form1.acctNo.value = "622588121251757643";
form1.acctName.value = "测试试";
form1.idNo.value = "440103198001010116";
form1.phoneNo.value = "13434245846";
break;
default:
form1.ibankCode.value = '00000000';
form1.acctNo.value = "6223250007268633";
form1.acctName.value = "测试";
form1.idNo.value = "210122198210120326";
}
form1.bankCode.value = form1.ibankCode.value;
}
var channelData = [
{code:"00000000",name:"--设置渠道的测试数据"},
{code:"RESET",name:"重置"},
{code:"00000000",name:"标准行"},
{code:"03090000CIB",name:"CIB -兴业银行"},
{code:"04145210STD",name:"HKB -汉口理财支付"},
{code:"04123330WZCB",name:"WZCB -温州银行"},
{code:"04012900BOS",name:"BOS -上海银行147258"},
{code:"01030000FPS",name:"FPS -农行生产签约"},
{code:"01050000FPS",name:"FPS -建行生产签约"},
{code:"03060000CGB",name:"CGB -广发协议支付"},
{code:"03080000EBPP",name:"EBPP -招行111111"},
{code:"03010000MBS",name:"MBS -交行代收付"},
{code:"01030000MBS",name:"MBS -农行代收付"},
{code:"01050000MBS",name:"MBS -建行代收付"},
{code:"00001003UHN",name:"UnionPay-湖南银联"},
{code:"00001010TLT",name:"TLT-通联通4通联宝"}
];
var bankCodeData = [
{code:"00000000",name:"--请选择发卡行"},
{code:"00000000",name:"标准行"},
{code:"01000000",name:"PSBC-邮政储蓄"},
{code:"01020000",name:"ICBC-工商银行"},
{code:"01030000",name:"ABC -农业银行"},
{code:"01040000",name:"BOC -中国银行"},
{code:"01050000",name:"CCB -建设银行"},
{code:"03010000",name:"COMM-交通银行"},
{code:"03020000",name:"CITIC中信银行"},
{code:"03030000",name:"CEB -光大银行"},
{code:"03040000",name:"HXB -华夏银行"},
{code:"03050000",name:"CMBC-民生银行"},
{code:"03060000",name:"CGB -广发银行"},
{code:"03080000",name:"CMB -招商银行"},
{code:"03090000",name:"CIB -兴业银行"},
{code:"03100000",name:"SPDB-浦发银行"},
{code:"03114560",name:"EGB -恒丰银行"},
{code:"04012900",name:"BOS -上海银行"},
{code:"04202220",name:"BANKOFDL大连"},
{code:"04416530",name:"CQCB-重庆银行"},
{code:"04123330",name:"WZCB-温州银行"},
{code:"04145210",name:"HKB -汉口银行"},
{code:"14136530",name:"CQRCB重庆农商"},
{code:"00001008",name:"UAC -新兴事业部-手机充值"},
{code:"00001009",name:"UAC -新兴事业部-缴费"},
{code:"00001010",name:"TLT-通联通4通联宝"},
{code:"12000001",name:"渠道12"}
];
var envIpData = [
{code:"192.168.103.121:8080",name:"192.168.103.121(DEV)"},
{code:"http://192.168.103.120",name:"192.168.103.120(DEV)"},
{code:"192.168.103.131:8080",name:"192.168.103.131(INT)"},
{code:"http://192.168.103.130",name:"192.168.103.130(INT)"},
{code:"192.168.103.141:8080",name:"192.168.103.141(STG)"},
{code:"http://192.168.103.140",name:"192.168.103.140(STG)"},
{code:"192.168.103.111:8080",name:"192.168.103.111(UAT)"},
{code:"http://192.168.103.110",name:"192.168.103.110(UAT)"},
{code:"http://127.0.0.1",name:"127.0.0.1(Local)"}
];
var initData = [{
id:'ibankCode',
type:'select',
data:bankCodeData
},{
id:'ipayerBankCode',
type:'select',
data:bankCodeData
},
{
id:'ipayeeBankCode',
type:'select',
data:bankCodeData
},
{
id:'retsetTestData',
type:'select',
data:channelData
},{
id:'sltServerIp',
type:'select',
data:envIpData
},{
type:'function',
data:function() {
if(document.form1 && document.form1.sltServerIp) {
var sltServerIp = document.location.protocol + "//" + document.location.host;
document.form1.sltServerIp.value=sltServerIp;
document.form1.serverIp.value=sltServerIp;
}
}
}
];
window.onload=function() {
for(var v=0; v<initData.length; v++) {
var vd = initData[v];
switch(vd.type) {
case 'select':
setOptions(vd.id,vd.data);
break;
case 'input':
break;
case 'function':
vd.data(vd);
break;
}
}
}
function setOptions(selEle,data) {
selEle = (document.form1 && document.form1[selEle])||document.getElementById(selEle);
if(!selEle) return;
selEle.length=0;//clear precious options
for(var i=0; i<data.length; i++) {
selEle[i] = new Option(data[i].name,data[i].code);
}
}
function $id(id) {
return typeof id == 'string'?document.getElementById(id):id;
}