package org.kaiwo.servlet;

import com.alibaba.fastjson.JSON;
import org.kaiwo.util.HttpTookit;
import org.kaiwo.util.SignUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.TreeMap;

/**
 * Created by chensk on 2018/8/4.
 * 愿我出走半生,归来仍是少年
 * 扫码支付
 */
@WebServlet("/scanpay")
public class ScanpayServlet  extends HttpServlet {

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TreeMap<String, String> map = new TreeMap<String, String>();
        map.put("orderId", req.getParameter("orderId"));
        map.put("mchtId", req.getParameter("mchtId"));
        map.put("transTime", req.getParameter("transTime"));
        map.put("transChannel", req.getParameter("transChannel"));
        map.put("frontUrl", req.getParameter("frontUrl"));
        map.put("notifyUrl", req.getParameter("notifyUrl"));
        map.put("tranAmt", req.getParameter("tranAmt"));
        map.put("currency", req.getParameter("currency"));
        map.put("transTime", req.getParameter("transTime"));
        map.put("remark", req.getParameter("remark"));
        map.put("deviceInfo", req.getParameter("deviceInfo"));
        map.put("deviceIp",req.getParameter("deviceIp"));
        map.put("goodsName", req.getParameter("goodsName"));
        map.put("goodsTag", req.getParameter("goodsTag"));
        map.put("sign", SignUtil.sign(map));
        resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().print(sendJson("http://47.105.152.87:8080/gatewaysite/p/scanpay",JSON.toJSONString(map)));
        resp.getWriter().close();
    }

    public String sendJson(String notifyUrl, String json) {

        try {
            byte[] postData = json.getBytes("UTF-8");
            URL url = new URL(notifyUrl);
            // 建立连接，设置连接的属性
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type","application/json");

            conn.setRequestProperty("Charset", "UTF-8");
            conn.setRequestProperty("Content-Length",
                    String.valueOf(postData.length));
            DataOutputStream outStream = new DataOutputStream(
                    conn.getOutputStream());
            outStream.write(postData);
            outStream.flush();
            outStream.close();
            // 将返回的输入流转换成字符串
            InputStream inputStream = conn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(
                    inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(
                    inputStreamReader);
            StringBuffer buffer = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }

            bufferedReader.close();
            inputStreamReader.close();
            // 释放资源
            inputStream.close();
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "nodata";
    }


}
