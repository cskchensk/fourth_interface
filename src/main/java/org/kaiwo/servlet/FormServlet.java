package org.kaiwo.servlet;

import org.apache.commons.lang3.StringUtils;
import org.kaiwo.util.SignUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by chensk on 2018/8/4.
 * 愿我出走半生,归来仍是少年
 * 网页支付
 */
@WebServlet("/pcsubmit")
public class FormServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");//客户端网页我们控制为UTF-8
        TreeMap<String, String> map = new TreeMap<String, String>();
        map.put("mchtId", req.getParameter("mchtId"));
        if(StringUtils.isNotBlank(req.getParameter("userId"))) {
            map.put("userId", req.getParameter("userId"));//04时候必传
        }
        if(StringUtils.isNotBlank(req.getParameter("payMode"))) {
            map.put("payMode", req.getParameter("payMode"));//07时必传
        }
        map.put("orderId", req.getParameter("orderId")); 

        map.put("transTime", System.currentTimeMillis()+"");
        map.put("transChannel", req.getParameter("transChannel"));
        map.put("notifyUrl", req.getParameter("notifyUrl"));
        map.put("frontUrl", req.getParameter("notifyUrl"));
        map.put("tranAmt", "1");
        map.put("goodsName",req.getParameter("goodsName"));
        map.put("goodsTag", req.getParameter("goodsTag"));
        map.put("deviceIp", req.getParameter("deviceIp"));
        map.put("sign", SignUtil.sign(map));
        req.setAttribute("maps",map);
        req.setAttribute("url","http://47.105.152.87:8080/gatewaysite/p/pcPay?");
        req.getRequestDispatcher("/WEB-INF/jsp/submit.jsp").forward(req,resp);

    }


}
