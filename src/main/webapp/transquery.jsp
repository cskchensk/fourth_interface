<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <script type="text/javascript" src="/resources/vue.min.js"></script>
    <script type="text/javascript" src="/resources/axios.min.js"></script>
    <script type="text/javascript" src="/resources/qs.min.js"></script>
    <title>交易查询接口</title>
</head>
<body id="vues">
<form name="form1" action="/scan" method="post" id="form1">
    交易查询接口<table>
    <tbody><tr></tr>
    <tr>
        <td>交易订单号：</td>
        <td><input type="text" id="orderId" name="orderId" value="{{orderId}}"   style="width:160px;"><b>*</b> <input type="button" value="刷新订单号"  v-on:click="setOrderNo"  ></td>
    </tr>
    <tr>

        <td>商户号：</td>
        <td><input type="text" id="mchtId" name="mchtId" style="width:160px;" value="{{mchtId}}"><b>*</b></td>
    </tr>
    <tr>
        <td>原交易时间：</td>
        <td><input type="text" id="transTime" name="transTime"  value="{{transTime}}" style="width:160px;"><b>*</b></td>
        <td>原交易流水号(重要）：</td>
        <td><input type="text" id="origOrderId" name="origOrderId" value="{{origOrderId}}" style="width:160px;"><b>*</b></td>
    </tr>
    </tbody></table>
    <input type="button" id="confirm" value="提交"  v-on:click="postData">

</form>
<div><textarea rows="10" cols="80" id="result">{{results}}</textarea></div>


<script>

    var vue = new Vue({
        el: '#vues',
        data: {
            mchtId: 'M81017000845',
            origOrderId:'20180806404',
            orderId:'',
            transTime:'',
            results:''
        },
        methods: {
            setOrderNo: function (event) {
                var curr = new Date();
                var m = curr.getMonth() + 1;
                if (m < 10) {
                    m = '0' + m;
                }
                var d = curr.getDate();
                if (d < 10) {
                    d = '0' + d;
                }
                var h = curr.getHours();
                if (h < 10) {
                    h = '0' + h;
                }
                var mi = curr.getMinutes();
                if (mi < 10) {
                    mi = '0' + mi;
                }
                var s = curr.getSeconds();
                if (s < 10) {
                    s = '0' + s;
                }
                this.transTime = '' + curr.getFullYear() + m + d + h + mi + s;
                this.orderId = '' + curr.getFullYear() + m + d + h + mi + s;

            },
            postData:function(){
                let postData = Qs.stringify({
                    mchtId: this.mchtId,
                    origOrderId:this.origOrderId,
                    orderId:this.origOrderId,
                    transTime:this.transTime,
                    origOrderId:this.origOrderId
                });
                let that = this;
                axios.post('/scan',postData)
                .then(function (response) {
                    console.log(response.data);
                    that.results=JSON.stringify(response.data);
                }).catch(function (error) {
                    console.log(error);
                });




            }


    }}

    ).setOrderNo();

</script>
</body></html>