package org.kaiwo.constants;

/**
 *
 * Created by Medivh on 2018/8/6.
 * 愿我出走半生,归来仍是少年
 *
 * 商户号 MT1610020001
 * 部门编号 1009
 *
 * boqi.cer为我公司提供
 * 私钥生成规则 拿去放到cmd运行就可以了  因为部门编号1009 所以 jks文件为 1009.jks
 * keytool -genkey -keyalg RSA -keysize 1024 -validity 365  -alias csii_key -keypass 1009@123 -keystore /boqi/key/1009.jks -storepass 1009@123
 * 公钥生成规则
 * keytool -export -alias csii_key -keystore /boqi/key/1009.jks -storepass  1009@123 -file /boqi/key/1009.cer keytool -genkey -validity 3650 -keystore keystore.jks -storepass storepassword -keypass keypassword -alias default -dname "CN=127.0.0.1, OU=MyOrgUnit, O=MyOrg, L=MyCity, S=MyRegion, C=MyCountry"
 *
 *
 *1144
 *
 */
public class Constants {

    public static String pu= "/boqi/key/mcht/1144.cer";//东又私钥路径
    public static String privateKeyUrl= "/boqi/key/mcht/1144.jks";//东又私钥路径
    public static String publicKeyUrl = "/boqi/key/mcht/boqi.cer";//商户公钥路径
    public static String password = "1144@123";
}
